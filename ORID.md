## O
- Today we learned how to simulate a request to test the API in a project.
- We split yesterday's code out of the service layer and turned the whole project into a three-tier structure to make the functionality of each part of the project more distinct.
- On code review I saw that many students used annotations, and I looked up the meaning of those annotations one by one to try to understand them.
## R
- It's confusing to see a lot of annotations.
## I
- We should have to clear memory before each test to avoid tests affecting each other.
- I learned how to learn to mock requests using the annotation @AutoConfigureMockMvc
- In order to avoid other layers of functionality impacting the current test, we often mock an object out and set its return value in advance.
## D
- I should think carefully about each layer of functionality in the project when coding.
