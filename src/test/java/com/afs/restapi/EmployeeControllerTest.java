package com.afs.restapi;

import com.afs.restapi.model.Employee;
import com.afs.restapi.repository.EmployeeRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class EmployeeControllerTest {
	@Autowired

	private EmployeeRepository employeeRepository;
	@Autowired
	MockMvc client;
	@BeforeEach
	void cleanEmpolyeeData(){
		employeeRepository.clearAll();
	}
	@Test
	void should_return_employees_when_getAllEmployee_given_employees() throws Exception {
		//Given
		Employee john = new Employee(null,"John Smith",32,"Male",5000.0);
		employeeRepository.insert(john);
		//when
		client.perform(MockMvcRequestBuilders.get("/employees"))     //then
				.andExpect(status().isOk())
				.andExpect(jsonPath("$",hasSize(1)))
				.andExpect(jsonPath("$[0].id").value(john.getId()))
				.andExpect(jsonPath("$[0].name").value(john.getName()))
				.andExpect(jsonPath("$[0].age").value(john.getAge()))
				.andExpect(jsonPath("$[0].gender").value(john.getGender()))
				.andExpect(jsonPath("$[0].salary").value(john.getSalary()));
	}
	@Test
	void should_return_employees_filter_by_gender_when_getEmployeeByGender_given_two_employees_with_different_gender() throws Exception {
		//Given
		Employee john = new Employee(null,"John Smith",32,"Male",5000.0);
		Employee emily = new Employee(null, "Jane Johnson", 28, "Female", 6000.0);
		employeeRepository.insert(john);
		employeeRepository.insert(emily);
		//when
		client.perform(MockMvcRequestBuilders.get("/employees").param("gender","Female"))
				//then
				.andExpect(status().isOk())
				.andExpect(jsonPath("$",hasSize(1)))
				.andExpect(jsonPath("$[0].id").value(emily.getId()))
				.andExpect(jsonPath("$[0].name").value(emily.getName()))
				.andExpect(jsonPath("$[0].age").value(emily.getAge()))
				.andExpect(jsonPath("$[0].gender").value(emily.getGender()))
				.andExpect(jsonPath("$[0].salary").value(emily.getSalary()));
	}
	@Test
	void should_return_created_employee_when_perform_insertEmployee_given_employee() throws Exception {
		//Given
		Employee john = new Employee(null,"John Smith",22,"Male",5000.0);
		String johnJson = new ObjectMapper().writeValueAsString(john);
		//when
		client.perform(MockMvcRequestBuilders.post("/employees")
						.contentType(MediaType.APPLICATION_JSON)
						.content(johnJson))
				//then
				.andExpect(status().isCreated())
				.andExpect(jsonPath("$.name").value(john.getName()))
				.andExpect(jsonPath("$.age").value(john.getAge()))
				.andExpect(jsonPath("$.gender").value(john.getGender()))
				.andExpect(jsonPath("$.salary").value(john.getSalary()))
				.andExpect(jsonPath("$.active").value(john.isActive()));
	}

	@Test
	void should_return_the_employee_when_perform_findEmployeeById_given_id() throws Exception {
		//Given
		Employee john = new Employee(null,"John Smith",32,"Male",5000.0);
		employeeRepository.insert(john);
		//when
		client.perform(MockMvcRequestBuilders.get("/employees/{id}",john.getId()))
				//then
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.name").value(john.getName()))
				.andExpect(jsonPath("$.age").value(john.getAge()))
				.andExpect(jsonPath("$.gender").value(john.getGender()))
				.andExpect(jsonPath("$.salary").value(john.getSalary()));
	}
	@Test
	void should_return_updated_employee_when_perform_updateEmployeeById_given_id_and_json() throws Exception {
		//Given
		Employee john = new Employee(null,"John Smith",32,"Male",5000.0);
		employeeRepository.insert(john);
		john.setAge(33);
		String johnJson = new ObjectMapper().writeValueAsString(john);
		//when
		client.perform(MockMvcRequestBuilders.put("/employees/{id}",john.getId())
				.contentType(MediaType.APPLICATION_JSON)
				.content(johnJson))
				//then
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.name").value(john.getName()))
				.andExpect(jsonPath("$.age").value(john.getAge()))
				.andExpect(jsonPath("$.gender").value(john.getGender()))
				.andExpect(jsonPath("$.salary").value(john.getSalary()));
	}
	@Test
	void should_return_null_when_perform_deleteEmployeeById_given_id()throws Exception{
		//Given
		Employee john = new Employee(null,"John Smith",32,"Male",5000.0);
		employeeRepository.insert(john);
		//when
		client.perform(MockMvcRequestBuilders.delete("/employees/{id}",john.getId()))
				//then
				.andExpect(status().isNoContent());
	}
	@Test
	void should_return_employees_when_getEmployeeBySizeAndPage_given_employees_and_size_page() throws Exception {
		//Given
		Employee John = new Employee(null,"John Smith",32,"Male",5000.0);
		Employee Jane = new Employee(null, "Jane Johnson", 28, "Female", 6000.0);
		Employee David = new Employee(null, "David Williams", 35,"Male", 5500.0);
		Employee Emily = new Employee(null, "Emily Brown", 23, "Female", 4500.0);
		Employee Michael = new Employee(null, "Michael Jones", 40, "Male", 7000.0);
		employeeRepository.insert(John);
		employeeRepository.insert(Jane);
		employeeRepository.insert(David);
		employeeRepository.insert(Emily);
		employeeRepository.insert(Michael);
		//when
		client.perform(MockMvcRequestBuilders.get("/employees")
						.param("page","2")
						.param("size","2"))     //then
				.andExpect(status().isOk())
				.andExpect(jsonPath("$",hasSize(2)))
				.andExpect(jsonPath("$[1].id").value(Emily.getId()))
				.andExpect(jsonPath("$[1].name").value(Emily.getName()))
				.andExpect(jsonPath("$[1].age").value(Emily.getAge()))
				.andExpect(jsonPath("$[1].gender").value(Emily.getGender()))
				.andExpect(jsonPath("$[1].salary").value(Emily.getSalary()));
	}
}
