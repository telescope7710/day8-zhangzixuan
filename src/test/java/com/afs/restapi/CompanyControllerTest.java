package com.afs.restapi;

import com.afs.restapi.model.Company;
import com.afs.restapi.model.Employee;
import com.afs.restapi.repository.CompanyRepository;
import com.afs.restapi.repository.EmployeeRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class CompanyControllerTest {
	@Autowired
	private CompanyRepository companyRepository;
	@Autowired
	private EmployeeRepository employeeRepository;
	@Autowired
	MockMvc client;
	@BeforeEach
	void cleanCompanyData(){
		companyRepository.clearAll();
	}
	@Test
	void should_return_companies_when_getAllCompanies_given_companies() throws Exception {
		//Given
		Company OOCL = new Company(null,"OOCL");
		Company Alibaba = new Company(null,"Alibaba");
		companyRepository.insert(OOCL);
		companyRepository.insert(Alibaba);
		//when
		client.perform(MockMvcRequestBuilders.get("/companies"))     //then
				.andExpect(status().isOk())
				.andExpect(jsonPath("$",hasSize(2)))
				.andExpect(jsonPath("$[0].id").value(OOCL.getId()))
				.andExpect(jsonPath("$[0].name").value(OOCL.getName()));
	}
	@Test
	void should_return_created_company_when_perform_insertCompany_given_company() throws Exception {
		//Given
		Company OOCL = new Company(null,"OOCL");
		String OOCLJson = new ObjectMapper().writeValueAsString(OOCL);
		//when
		client.perform(MockMvcRequestBuilders.post("/companies")
						.contentType(MediaType.APPLICATION_JSON)
						.content(OOCLJson))
				//then
				.andExpect(status().isCreated())
				.andExpect(jsonPath("$.name").value(OOCL.getName()));
	}
	@Test
	void should_return_the_company_when_perform_getCompanyById_given_id_and_companies() throws Exception {
//Given
		Company OOCL = new Company(null,"OOCL");
		Company Alibaba = new Company(null,"Alibaba");
		companyRepository.insert(OOCL);
		companyRepository.insert(Alibaba);
		//when
		client.perform(MockMvcRequestBuilders.get("/companies/{id}",Alibaba.getId()))     //then
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.id").value(Alibaba.getId()))
				.andExpect(jsonPath("$.name").value(Alibaba.getName()));
	}
	@Test
	void should_return_updated_company_when_perform_updateCompanyById_given_id_and_json() throws Exception {
		//Given
		Company OOCL = new Company(null,"OOCL");
		companyRepository.insert(OOCL);
		OOCL.setName("OOCLL");
		String OOCLJson = new ObjectMapper().writeValueAsString(OOCL);
		//when
		client.perform(MockMvcRequestBuilders.put("/companies/{id}",OOCL.getId())
				.contentType(MediaType.APPLICATION_JSON)
				.content(OOCLJson))
				//then
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.name").value(OOCL.getName()));
	}
	@Test
	void should_return_null_when_perform_deleteCompanyById_given_id()throws Exception{
		//Given
		Company OOCL = new Company(null,"OOCL");
		companyRepository.insert(OOCL);
		//when
		client.perform(MockMvcRequestBuilders.delete("/companies/{id}",OOCL.getId()))
				//then
				.andExpect(status().isNoContent());
	}
	@Test
	void should_return_companies_when_getCompaniesBySizeAndPage_given_companies_and_size_page() throws Exception {
		//Given
		Company John = new Company(null,"John Smith");
		Company Jane = new Company(null, "Jane Johnson");
		Company David = new Company(null, "David Williams");
		Company Emily = new Company(null, "Emily Brown");
		Company Michael = new Company(null, "Michael Jones");
		companyRepository.insert(John);
		companyRepository.insert(Jane);
		companyRepository.insert(David);
		companyRepository.insert(Emily);
		companyRepository.insert(Michael);
		//when
		client.perform(MockMvcRequestBuilders.get("/companies")
						.param("page","2")
						.param("size","2"))     //then
				.andExpect(status().isOk())
				.andExpect(jsonPath("$",hasSize(2)))
				.andExpect(jsonPath("$[1].id").value(Emily.getId()))
				.andExpect(jsonPath("$[1].name").value(Emily.getName()));
	}
	@Test
	void should_return_employees_when_getEmployeeByCompanyId_given_employees_and_companyId() throws Exception{
		Company OOCL = new Company(null,"OOCL");
		Company Alibaba = new Company(null,"Alibaba");
		companyRepository.insert(OOCL);
		companyRepository.insert(Alibaba);
		Employee John = new Employee(null,"John Smith",32,"Male",5000.0);
		Employee Jane = new Employee(null, "Jane Johnson", 28, "Female", 6000.0);
		Employee David = new Employee(null, "David Williams", 35,"Male", 5500.0);
		Employee Emily = new Employee(null, "Emily Brown", 23, "Female", 4500.0);
		Employee Michael = new Employee(null, "Michael Jones", 40, "Male", 7000.0);
		Jane.setCompanyId(OOCL.getId());
		Emily.setCompanyId(OOCL.getId());
		Michael.setCompanyId(OOCL.getId());
		John.setCompanyId(Alibaba.getId());
		David.setCompanyId(Alibaba.getId());
		employeeRepository.insert(John);
		employeeRepository.insert(Jane);
		employeeRepository.insert(David);
		employeeRepository.insert(Emily);
		employeeRepository.insert(Michael);

		//when
		client.perform(MockMvcRequestBuilders.get("/companies/{id}/employees",OOCL.getId()))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$",hasSize(3)))
				.andExpect(jsonPath("$[1].id").value(Emily.getId()))
				.andExpect(jsonPath("$[1].name").value(Emily.getName()))
				.andExpect(jsonPath("$[1].age").value(Emily.getAge()))
				.andExpect(jsonPath("$[1].gender").value(Emily.getGender()))
				.andExpect(jsonPath("$[1].salary").value(Emily.getSalary()));
	}
}
