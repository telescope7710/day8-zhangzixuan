package com.afs.restapi;

import com.afs.restapi.exception.AgeNotValidException;
import com.afs.restapi.exception.EmployeeNotActiveException;
import com.afs.restapi.exception.SalaryAndAgeNotMatchException;
import com.afs.restapi.model.Employee;
import com.afs.restapi.repository.EmployeeRepository;
import com.afs.restapi.service.EmployeeService;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

public class EmployeeServiceTest {

    EmployeeRepository mockedEmployeeRepository = mock(EmployeeRepository.class);

    @Test
    void should_return_not_valid_exception_when_perform_insertEmployee_given_employee_age_is_invalid() {
        EmployeeService employeeService = new EmployeeService(mock(EmployeeRepository.class));
        Employee employee = new Employee(1L, "John Smith", 2, "Male", 5000.0);

        assertThrows(AgeNotValidException.class, () -> employeeService.createEmployee(employee));
    }

    @Test
    void should_return_created_employee_when_perform_insertEmployee_given_employee_age_is_valid() {
        EmployeeService employeeService = new EmployeeService(mockedEmployeeRepository);
        Employee employee = new Employee(1L, "John Smith", 22, "Male", 5000.0);
        given(mockedEmployeeRepository.insert(any())).willReturn(employee);

        assertEquals(employeeService.createEmployee(employee), employee);
    }

    @Test
    void should_throw_salary_and_age_not_match_exception_when_perform_insertEmployee_given_employee_age_is_over_30_and_salary_is_below_20000() {
        EmployeeService employeeService = new EmployeeService(mockedEmployeeRepository);
        Employee employee = new Employee(1L, "John Smith", 32, "Male", 5000.0);

        assertThrows(SalaryAndAgeNotMatchException.class, () -> employeeService.createEmployee(employee));
    }

    @Test
    void should_set_inactive_when_delete_employee_given_an_active_employee() {
        EmployeeService employeeService = new EmployeeService(mockedEmployeeRepository);
        Employee employee = new Employee(1L, "John Smith", 2, "Male", 5000.0);
        given(mockedEmployeeRepository.findById(any())).willReturn(employee);

        employeeService.deleteEmployee(1L);
        verify(mockedEmployeeRepository, times(1)).delete(1L);
    }

    @Test
    void should_throw_not_active_employee_exception_when_perform_updateEmployee_given_employee_is_inactive() {
        EmployeeService employeeService = new EmployeeService(mockedEmployeeRepository);
        Employee employee = new Employee(1L, "John Smith", 32, "Male", 5000.0);
        employee.setActive(false);
        given(mockedEmployeeRepository.findById(any())).willReturn(employee);

        assertThrows(EmployeeNotActiveException.class, () -> employeeService.updateEmployee(1L, employee));
    }
}
