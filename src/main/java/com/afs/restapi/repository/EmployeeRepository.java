package com.afs.restapi.repository;

import com.afs.restapi.exception.EmployeeNotFoundException;
import com.afs.restapi.model.Employee;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

@Repository
public class EmployeeRepository {

    private final List<Employee> employees = new ArrayList<>();
    private final AtomicLong atomicID = new AtomicLong(0);

    public List<Employee> findAll() {
        return employees;
    }

    public Employee findById(Long id) {
        return employees.stream()
                .filter(employee -> employee.getId().equals(id))
                .findFirst()
                .orElseThrow(EmployeeNotFoundException::new);
    }

    public List<Employee> findByGender(String gender) {
        return employees.stream()
                .filter(employee -> employee.getGender().equals(gender))
                .collect(Collectors.toList());
    }

    public List<Employee> findByPageAndSize(Integer page, Integer size) {
        return employees.stream()
                .skip((long) (page - 1) * size)
                .limit(size)
                .collect(Collectors.toList());
    }

    public Employee insert(Employee newEmployee) {
        newEmployee.setId(atomicID.incrementAndGet());
        employees.add(newEmployee);
        return newEmployee;
    }

    public Employee update(Long id, Employee employee) {
        return employees.stream()
                .filter(employeeInList -> employeeInList.getId().equals(id))
                .findFirst()
                .orElseThrow(EmployeeNotFoundException::new).update(employee);
    }

    public void delete(Long id) {
        employees.stream()
                .filter(employeeInList -> employeeInList.getId().equals(id))
                .findFirst()
                .orElseThrow(EmployeeNotFoundException::new).setActive(false);
    }
    public List<Employee> getByCompanyId(Long companyId) {
        return employees.stream().filter(employee -> employee.getCompanyId().equals(companyId)).collect(Collectors.toList());
    }
    public void clearAll() {
        employees.clear();
    }
}

