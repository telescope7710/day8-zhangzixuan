package com.afs.restapi.repository;

import com.afs.restapi.exception.CompanyNotFoundException;
import com.afs.restapi.model.Company;
import com.afs.restapi.model.Employee;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

@Repository
public class CompanyRepository {
    private final List<Company> companies = new ArrayList<>();
    private final AtomicLong atomicID = new AtomicLong(0);
    public List<Company> findAll() {
        return companies;
    }

    public Company findById(Long id) {
        return companies.stream()
                .filter(company -> company.getId().equals(id))
                .findFirst()
                .orElseThrow(CompanyNotFoundException::new);
    }


    public List<Company> findByPageAndSize(Integer page, Integer size) {
        return companies.stream()
                .skip((long) (page - 1) * size)
                .limit(size)
                .collect(Collectors.toList());
    }

    public Company insert(Company company) {
        company.setId(atomicID.incrementAndGet());
        companies.add(company);
        return company;
    }

    public Company update(Long id, Company company) {
        companies.stream()
                .filter(companyInList -> companyInList.getId().equals(id))
                .findFirst()
                .orElseThrow(CompanyNotFoundException::new).update(company);
        return companies.stream()
                .filter(companyInList -> companyInList.getId().equals(id))
                .findFirst()
                .orElseThrow(CompanyNotFoundException::new);
    }

    public void delete(Long id) {
        companies.removeIf(company -> company.getId().equals(id));
    }

    public void clearAll() {
        companies.clear();
    }
}
