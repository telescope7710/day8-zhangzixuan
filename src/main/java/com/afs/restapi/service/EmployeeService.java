package com.afs.restapi.service;

import com.afs.restapi.exception.AgeNotValidException;
import com.afs.restapi.exception.EmployeeNotActiveException;
import com.afs.restapi.exception.EmployeeNotFoundException;
import com.afs.restapi.exception.SalaryAndAgeNotMatchException;
import com.afs.restapi.model.Employee;
import com.afs.restapi.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeService {
    @Autowired
    private final EmployeeRepository employeeRepository;

    public EmployeeService(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    public List<Employee> findAll() {
        return employeeRepository.findAll();
    }

    public Employee getEmployeeById(Long id) {
        return employeeRepository.findById(id);
    }

    public List<Employee> findEmployeesByGender(String gender) {
        return employeeRepository.findByGender(gender);
    }

    public List<Employee> findEmployeesByPageAndSize(Integer page, Integer size) {
        return employeeRepository.findByPageAndSize(page, size);
    }

    public Employee createEmployee(Employee employee) {
        if(employee.getAge() >= 18 && employee.getAge() <= 65){
            if(employee.getAge()>=30&&employee.getSalary()<20000){
                throw new SalaryAndAgeNotMatchException();
            }else
                return employeeRepository.insert(employee);
        }

        throw new AgeNotValidException();
    }

    public Employee updateEmployee(Long id, Employee employee) {
        if (employeeRepository.findById(id) == null) {
            throw new EmployeeNotFoundException();
        }
        if (!employeeRepository.findById(id).isActive()) {
            throw new EmployeeNotActiveException();
        }
        Employee employeeToUpdate = getEmployeeById(id);
        return employeeRepository.update(id, new Employee(employeeToUpdate.getId(), employeeToUpdate.getName(), employee.getAge(), employeeToUpdate.getGender(), employee.getSalary()));
    }

    public void deleteEmployee(Long id) {
        if (employeeRepository.findById(id) == null) {
            throw new EmployeeNotFoundException();
        }
        employeeRepository.delete(id);
    }

    public void clearAll() {
        employeeRepository.clearAll();
    }

    public List<Employee> getEmployeesByCompanyId(Long companyId) {
        return employeeRepository.getByCompanyId(companyId);
    }
}
