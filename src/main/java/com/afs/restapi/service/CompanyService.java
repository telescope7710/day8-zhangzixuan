package com.afs.restapi.service;

import com.afs.restapi.exception.CompanyNotFoundException;
import com.afs.restapi.exception.EmployeeNotFoundException;
import com.afs.restapi.model.Company;
import com.afs.restapi.model.Employee;
import com.afs.restapi.repository.CompanyRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CompanyService {
    private final CompanyRepository companyRepository;

    public CompanyService(CompanyRepository companyRepository) {
        this.companyRepository = companyRepository;
    }

    public List<Company> getAllCompanies() {
        return companyRepository.findAll();
    }


    public Company getCompanyById(Long id) {
        return companyRepository.findById(id);
    }

    public List<Company> findCompaniesByPageAndSize(Integer page, Integer size) {
        return companyRepository.findByPageAndSize(page, size);
    }

    public Company createCompany(Company company) {
        return companyRepository.insert(company);
    }

    public Company updateCompany(Long id, Company company) {
        if (companyRepository.findById(id) == null) {
            throw new CompanyNotFoundException();
        }
        return companyRepository.update(id, new Company(id, company.getName()));
    }
    public void deleteCompany(Long id) {
        if (companyRepository.findById(id) == null) {
            throw new EmployeeNotFoundException();
        }
        companyRepository.delete(id);
    }

    public void clearAll() {
        companyRepository.clearAll();
    }
}
