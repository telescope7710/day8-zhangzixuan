package com.afs.restapi.exception;

public class SalaryAndAgeNotMatchException extends RuntimeException{
    public SalaryAndAgeNotMatchException() {
        super("employee salary and age not match");
    }
}
