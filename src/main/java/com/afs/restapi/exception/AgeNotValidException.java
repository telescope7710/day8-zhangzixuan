package com.afs.restapi.exception;

public class AgeNotValidException extends RuntimeException{
    public AgeNotValidException() {
        super("employee age not valid");
    }
}
