package com.afs.restapi.exception;

public class EmployeeNotActiveException extends RuntimeException{
    public EmployeeNotActiveException() {
        super("employee is not active");
    }
}
